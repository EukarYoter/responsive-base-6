Mobile category navigation for medium-down widths

*Note:* This module is a complement to cat-nav-*, used for size small & medium

**Compatible modules**

- ``cat-nav-vert-mega-dd``
- ``cat-nav-vert-vert-down``
- ``cat-nav-vert-right``
