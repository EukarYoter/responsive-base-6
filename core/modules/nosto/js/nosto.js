//
//  This file is needed for Nosto recommendations in Jetshop v68
//
var Nosto = {
    version: "1.0.0",
    settings: {
        showOnlyOneRow: true
    },
    pages: {
        checkout: function () {
            var nostoDivs = Nosto.helpers.render([
                "cartpage-nosto-1"
            ]);
            $("html.page-responsive-checkout #main-area").after(nostoDivs);
        },
        error: function () {
            var nostoDivs = Nosto.helpers.render([
                "pagetemplate-nosto-1",
                "pagetemplate-nosto-2"
            ]);
            $("html.page-errorpage .errorPage").after(nostoDivs);
        },
        start: function () {
            var nostoDivs = Nosto.helpers.render([
                "frontpage-nosto-1"
            ]);
            $("#main-area").append(nostoDivs);
        },
        product: function () {
            var nostoDivs = Nosto.helpers.render([
                "productpage-nosto-1",
                "productpage-nosto-2"
            ]);
            $("#ctl00_main_ctl00_ctl00_pnlProductPanel").append(nostoDivs);
        },
        category: function () {
            var nostoDivs = Nosto.helpers.render([
                "productcategory-nosto-1"
            ]);
            $("html.page-listproducts .category-header-wrapper").after(nostoDivs);
            nostojs(function (api) {
                api.listen("postrender", function () {
                    $("#productcategory-nosto-1").addClass("nosto-complete")
                });
            });
        }
    },
    helpers: {
        complete: function () {
            $(window).trigger("nosto-complete");
            Nosto.helpers.hideRows();
        },
        hideRows: function () {
            if (Nosto.settings.showOnlyOneRow) {
                var nostoElementsList = $(".nosto_element");
                for (var i = 0; i < nostoElementsList.length; i++) {
                    var obj = nostoElementsList[i];
                    var selector = "#" + obj.getAttribute('id') + " ul li";
                    $(selector).each(function () {
                        $(this).show();
                        if ($(this).prev().length > 0) {
                            if ($(this).position().top != $(this).prev().position().top) {
                                $(this).hide();
                            }
                        }
                    });
                }
            }
        },
        render: function (nostoDivs) {
            var string = '<div class="nosto_wrapper">';
            for (var div in nostoDivs) {
                string += '<div class="nosto_element" id="' + nostoDivs[div] + '"></div>'
            }
            string += "</div>";
            return string;
        }
    }
};

J.pages.addToQueue("all-pages", Nosto.pages.error);
J.pages.addToQueue("category-page", Nosto.pages.category);
J.pages.addToQueue("product-page", Nosto.pages.product);
J.pages.addToQueue("start-page", Nosto.pages.start);
//J.pages.addToQueue("checkout-page", Nosto.pages.checkout);

// Weird construction to avoid running hideRows multiple times before recommendations are loaded
$(window).on("nosto-complete", function () {
    $(window).resize(function () {
        Nosto.helpers.hideRows();
    });
});
